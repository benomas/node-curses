let buffer = Buffer.alloc(1);

const modes = {
  '1':function() {
    let buffer = Buffer.alloc(1)
    console.log(buffer)
  },
  '2':function() {
    let buffer = Buffer.alloc(4)
    console.log(buffer)
  },
  '3':function() {
    let buffer = Buffer.from([1,2,5])
    console.log(buffer)
  },
  '4':function() {
    let buffer = Buffer.from('hola')
    console.log(buffer)
    console.log(buffer.toString())
  },
  '5':function() {
    let abc = Buffer.alloc(26)

    for (let i=0; i < 26; i++) {
      abc[i] = i + 97
    }

    console.log(abc.toString())
  },
}

let caseToCheck = 5

modes[caseToCheck]()
