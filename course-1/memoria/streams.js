const fs = require ('fs')
const stream = require ('stream')
const util = require ('util')

const modes = {
  '1':function() {
    let data = ''

    let readableStream = fs.createReadStream(`${__dirname}/input.txt`)

    readableStream.on('data', (chunk) => {
      console.log(chunk.toString())
    })
  },
  '2':function() {
    let data = ''

    let readableStream = fs.createReadStream(`${__dirname}/input.txt`)
    readableStream
      .setEncoding('utf8')
      .on('data', (chunk) => {
        data += chunk
      })
      .on('end', () => {
        console.log(data)
      })
  },
  '3':function() {
    process.stdout.write('hola')
    process.stdout.write('que')
    process.stdout.write('tal')
  },
  '4':function() {
    let readableStream = fs.createReadStream(`${__dirname}/input.txt`)
    const Transform = stream.Transform

    function ToUpper() {
      Transform.call(this)
    }

    util.inherits(ToUpper, Transform)

    ToUpper.prototype._transform = function(chunk, encoding,cb) {
      upperChunk = chunk.toString().toUpperCase()
      this.push(upperChunk)
      cb()
    }

    let toUpper = new ToUpper()

    readableStream
      .pipe(toUpper)
      .pipe(process.stdout)
  }
}

let caseToCheck = 4

modes[caseToCheck]()
