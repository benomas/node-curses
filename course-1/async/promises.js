function hello (name) {
    return new Promise ((resolve,reject)=>{
        setTimeout(function () {
            console.log(`Hello ${name}...`)
            resolve(name)
        },1000)
    })
}

function speak(name){
    return new Promise ((resolve,reject)=>{
        setTimeout(function () {
            console.log('bla, bla, bla...')
            //resolve(name)
            reject('fake error')
        },1000)
    })
}

function bye (name) {
    return new Promise ((resolve,reject)=>{
        setTimeout(function () {
            console.log(`Bye ${name}...`)
            resolve(name)
        },1000)
    })
}

console.log('Iniciando proceso')

hello('Carlos')
    .then(speak)
    .then(speak)
    .then(speak)
    .then(bye)
    .then((nombre)=>{
        console.log('proceso terminado')
    }).catch(error=>{
        console.log('an error occurred')
        console.log(error)
    })