function hello (name,externalCallback) {
    setTimeout(function () {
        console.log(`Hello ${name}...`)
        externalCallback(name)
    },1000)
}

function speak(callBackSpeak){
    setTimeout(function () {
        console.log('bla, bla, bla...')
        callBackSpeak()
    },1000)
}

function bye (name,externalCallback) {
    setTimeout(function () {
        console.log(`Bye ${name}...`)
        externalCallback()
    },1000)
}

function talk (nombre, interactions, callBack) {
    if(interactions >=0){
        speak(function(){
            talk(nombre, --interactions, callBack)
        })
    }else{
        callBack(nombre,callBack)
    }
}

console.log('start...')
hello('Jonh',function(user){
    talk(user,3,function(){
        console.log('callBackProccess completed')
    })
})

console.log('end...')