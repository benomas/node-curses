async function hello (name) {
    return new Promise ((resolve,reject)=>{
        setTimeout(function () {
            console.log(`Hello ${name}...`)
            resolve(name)
        },1000)
    })
}

async function speak(name){
    return new Promise ((resolve,reject)=>{
        setTimeout(function () {
            console.log('bla, bla, bla...')
            resolve(name)
            //reject('fake error')
        },1000)
    })
}

async function bye (name) {
    return new Promise ((resolve,reject)=>{
        setTimeout(function () {
            console.log(`Bye ${name}...`)
            resolve(name)
        },1000)
    })
}

async function main(){
    try{
        let nombre  = await hello('jose')
        await speak(nombre)
        await speak(nombre)
        await speak(nombre)
    }catch(error){
        console.log('an error occurred')
        console.log(error)
    }
}

console.log('Iniciando proceso')
main()
console.log('continuar')
/*
hello('Carlos')
    .then(speak)
    .then(speak)
    .then(speak)
    .then(bye)
    .then((nombre)=>{
        console.log('proceso terminado')
    }).catch(error=>{
        console.log('an error occurred')
        console.log(error)
    })*/