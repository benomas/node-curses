const bcrypt = require('bcrypt');

const password = '123Segura!'

bcrypt.hash(password,5, (error,hash) => {
  console.table({
    error,
    hash
  })

  bcrypt.compare(`${password}`,hash,(error,res) => {
    console.table({
      error,
      res
    })
  })
})
