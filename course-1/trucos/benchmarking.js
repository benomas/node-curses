
const modes = {
  '1':function() {
    let suma = 0
    console.time('bucle')

    for (let i=0; i < 10000000; i++){
      suma ++
    }

    console.timeEnd('bucle')
  },
  '2':function() {
    let suma = 0
    console.time('script')
    console.time('bucle1')

    for (let i=0; i < 10000000; i++){
      suma ++
    }

    console.timeEnd('bucle1')

    suma = 0
    console.time('bucle2')

    for (let j=0; j < 10000000; j++){
      suma ++
    }

    console.timeEnd('bucle2')
    console.timeEnd('script')
  },
  '3':function() {
    let suma = 0
    console.time('script')
    console.time('bucle1')

    for (let i=0; i < 10000000; i++){
      suma ++
    }

    console.timeEnd('bucle1')

    suma = 0
    console.time('bucle2')

    for (let j=0; j < 10000000; j++){
      suma ++
    }

    console.timeEnd('bucle2')
    console.time('asincrono')
    asincrona().then(()=>{
      console.timeEnd('asincrono')
    })
    console.timeEnd('script')

    function asincrona(){
      return new Promise(resolve => {
        setTimeout(() => {
          resolve()
        })
      })
    }
  }
}

let caseToCheck = 3

modes[caseToCheck]()
