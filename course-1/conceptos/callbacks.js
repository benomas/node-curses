function hello (name,externalCallback) {
    setTimeout(function () {
        console.log(`Hello ${name}...`)
        externalCallback(name)
    },1000)
}

function bye (name,externalCallback) {
    setTimeout(function () {
        console.log(`Bye ${name}...`)
        externalCallback()
    },1000)
}

console.log('start...')

hello('Jonh',function(user){
    bye(user,function(){
        console.log('delayed callBack')
    })
})

console.log('end...')