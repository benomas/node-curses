const fs =  require('fs');

function readFile (path) {
    return new Promise ((resolve,reject)=>{
        fs.readFile(path,(err,data)=> {
            if (err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}

function writeFile (path, content) {
    return new Promise ((resolve,reject)=>{
        fs.writeFile(path,content,(err,data)=> {
            if (err){
                reject(err)
            }else{
                resolve('ok')
            }
        })
    })
}

function deleteFile (path, content) {
    return new Promise ((resolve,reject)=>{
        fs.unlink(path,(err,data)=> {
            if (err){
                reject(err)
            }else{
                resolve('ok')
            }
        })
    })
}

function fileToText (fileData) {
    return fileData.toString()
}

function upperCase (fileText) {
    return fileText.toUpperCase()
}

async function main(){
    try{
        const filePath =`${__dirname}/example-file.txt`
        await writeFile(filePath,`${await readFile(filePath).then(fileToText)} \n extra content`)
        //
        await deleteFile(filePath)
        console.log(`The file was rewrited`)
    }catch(error){
        console.log('my custom error')
        console.log(error)
    }
    console.log('starting my script')
}

main()
/*
writeFile(`${__dirname}/example-file.txt`,'rewrited file content', function(output){
    console.log(output)
})
readFile(`${__dirname}/example-file.txt`, function(output){
    console.log(output)
})*/