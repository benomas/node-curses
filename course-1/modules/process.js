process.on('uncaughtException',(error,origen)=>{
  console.log('Excepcion ignorada')
  console.error(error.message)
  console.error(origen)
})

process.on('beforeExit',()=>{
  console.log('Terminando proceso')
})

process.on('exit',()=>{
  console.log('Proceso terminado')
})


