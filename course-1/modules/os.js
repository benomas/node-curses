const os = require ('os')

const SIZE = 1014
function kb (bytes) { return bytes / SIZE}
function mb (bytes) { return kb(bytes) / SIZE}
function gb (bytes) { return mb(bytes) / SIZE}

const modes = {
  '1':function() {console.log(os.arch())},
  '2':function() {console.log(os.cpus())},
  '3':function() {console.log(os.constants)},
  '4':function() {
    console.log(os.freemem())
    console.log(kb(os.freemem()))
    console.log(mb(os.freemem()))
    console.log(gb(os.freemem()))
  },
  '5':function() {console.log(os.homedir())},
  '6':function() {console.log(os.tmpdir())},
  '7':function() {console.log(os.hostname())},
  '8':function() {console.log(os.networkInterfaces())},
}

modes[8]()
