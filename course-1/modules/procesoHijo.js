const { exec,spawn } = require('child_process')

function exec1() {
    exec('ls -la',(err,stdout,sterr) => {
        if (err){
            return console.log(err)
        }
    
        console.log(stdout)
    })
}

function exec2() {
    exec('node console.js',(err,stdout,sterr) => {
        if (err){
            return console.log(err)
        }
    
        console.log(stdout)
    })
}

function exec3() {
    let proceso = spawn('ls',['la'])
    console.log(proceso.pid)
    console.log(proceso.connected)

    proceso.stdout.on('data',data => data.toString())

    proceso.on('exit',()=> {
        console.log(`ha terminado el proceso? ¡${proceso.killed === false ? 'No':'Si'}!`)
    })
}

//exec1()
//exec2()
exec3()