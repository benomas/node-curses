const puppeteer = require('puppeteer');

const modes = {
  '1':function() {
    (async () => {
      console.log('inicia')
      const browser = await puppeteer.launch()
      console.log('Cerramos navegador')
      browser.close()
      console.log('Navegador cerrado')
    })()
  },
  '2':function() {
    (async () => {
      console.log('inicia')
      const browser = await puppeteer.launch({headless:false})
      const page =  await browser.newPage()
      await page.goto('https://es.wikipedia.org/wiki/Node.js')
      console.log('Cerramos navegador')
      browser.close()
      console.log('Navegador cerrado')
    })()
  },
  '3':function() {
    (async () => {
      console.log('inicia')
      const browser = await puppeteer.launch({headless:false})
      const page =  await browser.newPage()
      await page.goto('https://es.wikipedia.org/wiki/Node.js')

      var titulo1 = await page.evaluate(()  => {
        const h1 = document.querySelector('h1')
        console.log(h1.innerHTML)
        return h1.innerHTML
      })

      console.log(titulo1)

      console.log('Cerramos navegador')
      browser.close()
      console.log('Navegador cerrado')
    })()
  }
}

let caseToCheck = 3

modes[caseToCheck]()
