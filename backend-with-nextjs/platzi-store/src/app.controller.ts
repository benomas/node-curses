import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return 'Hola tu';
  }

  @Get('otro-end-point')
  newEndPoint() {
    return 'otro endpoint';
  }

  @Get('/ruta/')
  hello() {
    return 'con /sas/';
  }

  @Get('products/:id')
  show(@Param() params: any) {
    return `product ${params.id}`;
  }

  @Get('categories/:id/products/:productId')
  gategoryShow(@Param('id') id: string, @Param('productId') productId: string) {
    return `category ${id} product ${productId}`;
  }
}
